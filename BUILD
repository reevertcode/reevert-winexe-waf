BUILD INSTRUCTIONS FOR DEBIAN (Updated: Feb 23, 2021)
####################################################

This is the build instructions for Debian 8 (jessie) and Debian 10 (buster).
We will provide the updated patches in the future for newer versions of Samba.

Successful Builds:
   - Samba 4.3.13 with Debian 8 and Debian 10 (SMB v1 and v2 - amd64)
   - Samba 4.3.13 with Debian 10 (SMB v1 and v2 - arm64)
   - Samba 4.7.12 with Debian 10 (SMB v2 only - amd64)
   - Samba 4.7.12 with Debian 10 (SMB v2 only - arm64)
   - Samba 4.8.12 with Debian 10 (SMB v2 only - amd64)
   - Samba 4.8.12 with Debian 10 (SMB v2 only - arm64)

Please note that compiled binaries on Debian 10 are compatible and work with with Debian 9.

1) Create a clean build chroot (Debian 8): debootstrap --arch="amd64" jessie debian-jessie/ http://archive.debian.org/debian/
   Create a clean build chroot (Debian 10): debootstrap --arch="amd64" buster debian-buster/ http://deb.debian.org/debian/

ARMv8 Note 1: Replace amd64 with arm64 when building on aarch64 (Raspberry Pi4 and similar 64bit ARMv8 architechture)
ARMv8 Note 2: In order to compile arm64 version on Debian 9/10, glibc packages need to be recompiled without the PIE flags. In debian/rules, change:

CC     = $(DEB_HOST_GNU_TYPE)-$(BASE_CC)$(DEB_GCC_VERSION) -no-pie -fno-PIE
CXX    = $(DEB_HOST_GNU_TYPE)-$(BASE_CXX)$(DEB_GCC_VERSION) -no-pie -fno-PIE

to:

CC     = $(DEB_HOST_GNU_TYPE)-$(BASE_CC)$(DEB_GCC_VERSION)
CXX    = $(DEB_HOST_GNU_TYPE)-$(BASE_CXX)$(DEB_GCC_VERSION)

Then recompile and re-install the packages before compiling winexe

2) Chroot and install required packages:

   # chroot debian-jessie OR # chroot debian-buster
   # apt-get install wget locales build-essential git gcc-mingw-w64 comerr-dev libpopt-dev libbsd-dev zlib1g-dev libc6-dev python3-dev libgnutls28-dev devscripts pkg-config autoconf libldap2-dev libtevent-dev libtalloc-dev libacl1-dev libpam0g-dev libarchive-dev python2.7-dev
   # apt-get clean

3) Get the sources:

   git clone https://bitbucket.org/reevertcode/reevert-winexe-waf.git
   wget https://download.samba.org/pub/samba/stable/samba-4.3.13.tar.gz (OR newer supported versions)

4) cd reevert-winexe-waf

5) tar -xf ../samba-4.3.13.tar.gz && mv samba-4.3.13 samba (OR newer supported versions)

6) rm -r source/smb_static

7) cat patches/fix_smb_static.patch | patch -p1

8) If building for SMBv2 (Samba 4.3 and 4.7+):
        cat patches/smb2_nognutls_noaddc.patch | patch -p1
        cat patches/smb2_add_public_includes_samba_4.3.patch | patch -p1 (Samba 4.3)
        OR
        cat patches/smb2_add_public_includes_samba_4.7.patch | patch -p1 (Samba 4.7+)

   If building for SMBv1 (Samba 4.3 only):
        cat patches/smb1_nognutls_noaddc.patch | patch -p1

8a) Debian 9/10 and Samba 4.3 only: cat patches/fix_samba_perl.py.patch | patch -p0

9) cd source && ln -s ../samba/bin/default/smb_static
        
10) ./waf --samba-dir=../samba configure build

===================================================================================================
Note for Samba 4.7.5+ : If the very last phase of the build process fails with the following error:

/bin/ld: /root/reevert-winexe-waf/source/smb_static/build/libsmb_static.a(access_80.o): in function `string_match':
access.c:(.text+0x412): undefined reference to `yp_get_default_domain'
collect2: error: ld returned 1 exit status

Apply this patch:

# cat patches/disable_yp_get_default_domain.patch | patch -p1
